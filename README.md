# lightpuck

A [PlatforIO](http://platformio.org) project based on the [Adafruit BLE Feather Lamp](https://learn.adafruit.com/ble-feather-lamp/overview).  This is the 'firmware' that runs on the Arduino-like Feather device.

Changes of note from the Adafruit example code:
 * Battery service
 * Different BLE advertisement device name
 
 
