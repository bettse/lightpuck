/*********************************************************************
  This is an example for our nRF51822 based Bluefruit LE modules

  Pick one up today in the adafruit shop!

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  MIT license, check LICENSE for more information
  All text above, and the splash screen below must be included in
  any redistribution
 *********************************************************************/

#include <string.h>
#include <Arduino.h>
#include <SPI.h>
#if not defined(_VARIANT_ARDUINO_DUE_X_) && not defined(_VARIANT_ARDUINO_ZERO_)
#include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BLEBattery.h"

#include "BluefruitConfig.h"

#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>

/*=========================================================================
  APPLICATION SETTINGS

      FACTORYRESET_ENABLE       Perform a factory reset when running this sketch
     
                                Enabling this will put your Bluefruit LE module
  in a 'known good' state and clear any config
  data set in previous sketches or projects, so
                                running this at least once is a good idea.
     
                                When deploying your project, however, you will
  want to disable factory reset by setting this
  value to 0.  If you are making changes to your
                                Bluefruit LE device via AT commands, and those
  changes aren't persisting across resets, this
  is the reason why.  Factory reset will erase
  the non-volatile memory where config data is
  stored, setting it back to factory default
  values.
         
                                Some sketches that require you to bond to a
  central device (HID mouse, keyboard, etc.)
  won't work at all with this feature enabled
  since the factory reset will clear all of the
  bonding data stored on the chip, meaning the
  central device won't be able to reconnect.
  PIN                       Which pin on the Arduino is connected to the NeoPixels?
  NUMPIXELS                 How many NeoPixels are attached to the Arduino?
  -----------------------------------------------------------------------*/
#define FACTORYRESET_ENABLE 1

#define VBATPIN A9
#define PIN 6
#define NUMPIXELS 24
#define BLINK1_REPORT_ID  1
#define BLINK1_REPORT_SIZE 8
#define BLINK1_BUF_SIZE (BLINK1_REPORT_SIZE+1)
#define COLOR_PATTERNS_SIZE 12
/*=========================================================================*/

struct RGB {
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
  uint8_t th = 0;
  uint8_t tl = 0;
  uint8_t n = 0;
};

struct Tickle {
  uint8_t on = 0;
  uint8_t th = 0;
  uint8_t tl = 0;
  uint8_t st = 0;
  uint8_t sp = 0;
  uint8_t ep = COLOR_PATTERNS_SIZE - 1;
};

struct PlayLoop {
  uint8_t on = 0;
  uint8_t sp = 0;
  uint8_t ep = COLOR_PATTERNS_SIZE - 1;
  uint8_t c = 0;
  uint8_t noop1 = 0;
  uint8_t noop2 = 0;
};

struct Packet {
  uint8_t report_id = 0;
  char command = ' ';
  union Paramaters {
    struct RGB rgb;
    struct Tickle tickle;
    struct PlayLoop playloop;
  } parameters;
};

union PacketBuffer {
  struct Packet packet;
  uint8_t buffer[sizeof(struct Packet)];
};

struct MyAnimationState {
  RgbColor StartingColor;  // the color the animation starts at
  RgbColor EndingColor; // the color the animation will end at
  AnimEaseFunction Easeing; // the acceleration curve it will use 
};

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);
Adafruit_BLEBattery battery(ble);

const uint16_t PixelCount = NUMPIXELS;
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(NUMPIXELS, PIN);
NeoPixelAnimator animations(PixelCount, NEO_MILLISECONDS);
MyAnimationState animationState[PixelCount];

int32_t version;

RGB colorPatterns[COLOR_PATTERNS_SIZE];
PlayLoop playing;
uint8_t playindex = 0;
Tickle tickle;
uint8_t currentBattery = 0;

// A small helper
void error(const __FlashStringHelper *err) {
  Serial.println(err);
  while (1);
}

void setup(void) {
  Serial.begin(115200);

  /* Initialise the module */
  Serial.print(F("Initialising the Bluefruit LE module: "));

  if (!ble.begin(VERBOSE_MODE)) {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println(F("OK!"));

  if (FACTORYRESET_ENABLE) {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if (!ble.factoryReset()) {
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();
  battery.begin(true);
  if (! ble.sendCommandCheckOK(F("AT+GAPDEVNAME=lightpuck")) ) {
    error(F("Could not set device name?"));
  }

  if (! ble.sendCommandWithIntReply(F("ATI=4"), &version) ) {
    error(F("Could not get version?"));
  }

  ble.verbose(false);
  ble.setMode(BLUEFRUIT_MODE_DATA);
  Serial.println("Setup complete");
}

void loop(void) {
  size_t len = 0;
  RgbColor color;
  PacketBuffer pb = {};

  updateBatteryPercentage();

  if (ble.isConnected()) {
    while (ble.available() && len < BLINK1_REPORT_SIZE) {
      uint8_t c = ble.read();
      pb.buffer[len++] = c;
    }
  }
  Packet packet = pb.packet;

  if (packet.report_id == 1) {
    Serial.print("New command: ");
    printHex(pb.buffer, len);

    switch (packet.command) {
      case 'c':
      case 'n':
        fadeToRGB(packet.parameters.rgb);
        break;
      case 'r':
        readRGB(packet);
        break;
      case 'D':
        setTickle(packet);
        break;
      case 'p':
        setPlayLoop(packet);
        break;
      case 'S':
        getPlayState(packet);
        break;
      case 'P':
        setColorPatternLine(packet);
        break;
      case 'R':
        getColorPatternLine(packet);
        break;
      case 'W':
        savePattern(packet);
        break;
      case 'v':
        getVersion(packet);
        break;
      case '!':
        test();
        break;
    }
  }

  animations.UpdateAnimations();
  strip.Show();
}

void setTickle(Packet packet) {
  Serial.println("Set Tickle");
  tickle = packet.parameters.tickle;
}

void setPlayLoop(Packet packet) {
  Serial.println("Set Play Loop");
  if (packet.parameters.playloop.ep > COLOR_PATTERNS_SIZE - 1) {
    packet.parameters.playloop.ep = COLOR_PATTERNS_SIZE - 1;
  }
  playing = packet.parameters.playloop;
}

void getPlayState(Packet packet) {
  Serial.println("Get Play State");
  uint8_t reply[BLINK1_BUF_SIZE] = {
    0x01,
    'S',
    playing.on,
    playing.sp,
    playing.ep,
    playing.c,
    playindex,
    0,
    '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void getColorPatternLine(Packet packet) {
  Serial.println("Get Color Pattern Line");
  RGB rgb = packet.parameters.rgb;
  RGB pattern = colorPatterns[rgb.n];
  uint8_t reply[BLINK1_BUF_SIZE] = {
    0x01,
    'R',
    pattern.r,
    pattern.b,
    pattern.b,
    pattern.th,
    pattern.tl,
    rgb.n,
    '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void setColorPatternLine(Packet packet) {
  Serial.println("Set Color Pattern Line");
  RGB rgb = packet.parameters.rgb;
  if (rgb.n < COLOR_PATTERNS_SIZE) {
    colorPatterns[rgb.n] = rgb;
  }
}

void savePattern(Packet packet) {
  Serial.println("Save Pattern");
  PacketBuffer pb = {};
  pb.packet = packet;
  if (pb.buffer[2] == 0xBE && pb.buffer[3] == 0xEF && pb.buffer[4] == 0xCA && pb.buffer[6] == 0xFE) {
  }
}

void getVersion(Packet packet) {
  Serial.println("Get Version");
  char reply[BLINK1_BUF_SIZE] = {0x01, packet.command, 0, 'B', '1', 0, 0, 0, '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void test() {
  Serial.println("Test command");
}

void fadeToRGB(RGB rgb) {
  Serial.println("Fade to RGB");
  int time = (rgb.th << 8 | rgb.tl) * 10;
  RgbColor c = RgbColor(rgb.r, rgb.g, rgb.b);

  switch (rgb.n) {
    case 0: //All
      for (uint16_t pixel = 0; pixel < PixelCount; pixel++) {
        fadePixel(pixel, c, time);
      }
      break;
    case 1: //Odds
      for (uint16_t pixel = 1; pixel < PixelCount; pixel = pixel + 2) {
        fadePixel(pixel, c, time);
      }
      break;
    case 2: //Evens
      for (uint16_t pixel = 0; pixel < PixelCount; pixel = pixel + 2) {
        fadePixel(pixel, c, time);
      }
      break;
    default:
      fadePixel(rgb.n, c, time);
  }
}

void fadePixel(uint8_t n, RgbColor c, uint16_t time) {
    animationState[n].StartingColor = strip.GetPixelColor(n);
    animationState[n].EndingColor = c;
    animationState[n].Easeing = NeoEase::Linear;
    animations.StartAnimation(n, time, AnimUpdate);
}

void readRGB(Packet packet) {
  Serial.println("Read RGB");
  RGB rgb = packet.parameters.rgb;
  RgbColor color = strip.GetPixelColor(rgb.n);
  uint8_t reply[BLINK1_BUF_SIZE] = {0x01, 'r', color.R, color.G, color.B, 0, 0, rgb.n, '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void AnimUpdate(const AnimationParam& param) {
  float progress = animationState[param.index].Easeing(param.progress);

  RgbColor updatedColor = RgbColor::LinearBlend(
      animationState[param.index].StartingColor,
      animationState[param.index].EndingColor,
      progress);
  strip.SetPixelColor(param.index, updatedColor);

  if (param.state == AnimationState_Completed) {
    if (playing.on) {
      RGB next = colorPatterns[playindex++];
      fadeToRGB(next);

      //When the loop ends
      if (playindex >= playing.ep) {
        Serial.println("Start next loop");
        playindex = playing.sp;
        playing.c--;
      }

      if (playing.c <= 0) {
        Serial.println("Stop playing");
        playing.on = 0;
      }
    }
  }
}

uint8_t batteryPercentage() {
  float measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage
  measuredvbat -= 3.5; //Subtract lowest value
  measuredvbat /= 0.7; //Divide by total range [4.2, 3.5]
  measuredvbat *= 100; //Convert to percentage
  return (uint8_t)measuredvbat;
}

void updateBatteryPercentage() {
  uint8_t newPercent = batteryPercentage();
  if (newPercent != currentBattery) {
    currentBattery = newPercent;
    battery.update(currentBattery);
  }
}

void printHex(const uint8_t *data, const uint32_t numBytes) {
  uint32_t szPos;
  for (szPos = 0; szPos < numBytes; szPos++) {
    Serial.print(F("0x"));
    // Append leading 0 for small values
    if (data[szPos] <= 0xF) {
      Serial.print(F("0"));
      Serial.print(data[szPos] & 0xf, HEX);
    } else {
      Serial.print(data[szPos] & 0xff, HEX);
    }
    // Add a trailing space if appropriate
    if ((numBytes > 1) && (szPos != numBytes - 1)) {
      Serial.print(F(" "));
    }
  }
  Serial.println();
}
